import random
import math
import os
import sys

from deap import base
from deap import creator
from deap import tools

from PyQt5.QtChart import QLineSeries, QChart, QValueAxis, QChartView

LEN = 10
NGEN = 1000

generations = []

solutions = []

SUM_TARGET = 36
PROD_TARGET = 360

stop_evolution = False

q_min_series = QLineSeries()
q_min_series.setName("MIN")
q_max_series = QLineSeries()
q_max_series.setName("MAX")
q_avg_series = QLineSeries()
q_avg_series.setName("AVG")

#individual = [1,3,4,5,6 | 2,7,8,9,10]      IDEAL IND
#individual = [1,2,3,4,5,6,7,8,9,10]        STARTING IND

def evaluateInd(individual):
    ssum = 0
    prod = 1
    for x in range(5):
        prod *= individual[x] + 1     
    for x in range(5, 10):
        ssum += individual[x] + 1
    scaled_sum_error = (ssum - SUM_TARGET) / SUM_TARGET
    scaled_prod_error = (prod - PROD_TARGET) / PROD_TARGET
    combined_error = math.fabs(scaled_sum_error) + math.fabs(scaled_prod_error)
    return combined_error,


class GeneticAlgorithm:       
    def run(self, population, mutation, elitism, iteration):
        global LEN
        
        creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
        creator.create("Individual", list, fitness=creator.FitnessMin)
        
        self.toolbox = base.Toolbox()
        
        self.toolbox.register("indices", random.sample, range(LEN), LEN)
        self.toolbox.register("individual", tools.initIterate, creator.Individual, 
                              self.toolbox.indices)
        self.toolbox.register("population", tools.initRepeat, 
                              list, self.toolbox.individual)
        
        self.toolbox.register("evaluate", evaluateInd)
        self.toolbox.register("mate", tools.cxUniformPartialyMatched, indpb=0.2)
        self.toolbox.register("mutate", tools.mutShuffleIndexes, indpb=0.2)
        self.toolbox.register("select", tools.selTournament, tournsize = 3)
        
        self.pop = self.toolbox.population(n=population)
        
        fitnesses = list(map(self.toolbox.evaluate, self.pop))
        for ind, fit in zip(self.pop, fitnesses):
            ind.fitness.values = fit
            
        self.fits = [ind.fitness.values[0] for ind in self.pop]
        
        return self.evolve(population, mutation, elitism, iteration)


    def evolve(self, population, mutation, elitism, iteration):
        global stop_evolution
        global q_min_series
        global q_max_series
        global q_avg_series
        
        global solutions
        global generations
        
        file_flag = True
        
        curr_g = 0
        while min(self.fits) != 0 and curr_g < NGEN:
            if stop_evolution:
                break
            
            curr_g += 1
            
            offspring = []
            offspring.append(self.toolbox.select(self.pop, 1)[0])
            for i in range(population - elitism - 1):
                while True:
                    new_o = self.toolbox.select(self.pop, 1)[0]
                    if new_o != offspring[len(offspring) - 1]:
                        offspring.append(new_o)
                        break
            
            offspring = list(map(self.toolbox.clone, offspring))            
            
            for child1, child2 in zip(offspring[::2], offspring[1::2]):
                self.toolbox.mate(child1, child2)
                del child1.fitness.values
                del child2.fitness.values
                
            for mutant in offspring:
                if random.random() < mutation:
                    self.toolbox.mutate(mutant)
                    del mutant.fitness.values
            
            offspring.extend(list(map(self.toolbox.clone, tools.selBest(self.pop, elitism))))
            
            invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
            fitnesses = map(self.toolbox.evaluate, invalid_ind)
            for ind, fit in zip(invalid_ind, fitnesses):
                ind.fitness.values = fit
            
            self.pop[:] = offspring
            
            self.fits = [ind.fitness.values[0] for ind in self.pop]
            
            length = len(self.pop)
            mean = sum(self.fits) / length
            sum2 = sum(x*x for x in self.fits)
            std = abs(sum2 / length - mean**2)**0.5
            
            q_min_series.append(curr_g, min(self.fits))
            q_max_series.append(curr_g, max(self.fits))
            q_avg_series.append(curr_g, mean)
            
           
        best_ind = tools.selBest(self.pop, 1)[0]
        if best_ind.fitness.values != (0.0,):
            q_min_series.clear()
            q_max_series.clear()
            q_avg_series.clear()
            return False 
        
        
        for i in range(10):
            best_ind[i]+=1
        solutions.append(best_ind)
        
        #filename = "perm/qs" + str(NO_QUEENS) + "-p" + str(population) + "-m" + str(mutation) + "-e" + str(elitism) + "-" + str(iteration) + ".csv"
        filename = "perm/len" + str(LEN) + "-p" + str(population) + "-m" + str(mutation) + "-e" + str(elitism) + "-" + str(iteration) + ".csv"
        
        with open(filename, 'w') as dat:
            for i in range(q_min_series.count()):
                dat.write('%f,%f\n' % (i, q_min_series.at(i).y()))
                    
        generations.append(curr_g)
        
        if iteration == 5:
            self.remove_non_avg_csvs(generations, population, mutation, elitism)
            generations = []
            
        q_min_series.clear()
        q_max_series.clear()
        q_avg_series.clear()
        return True
    
    def remove_non_avg_csvs(self, generations, population, mutation, elitism):
            print("---------------------------------------------")
            print("Population: %s,  mutations: %s,  elitism: %s    \ngenerations: %s" % (population, mutation, elitism, generations), end="    ")
            
            _sum = 0
            for generation in generations:
                _sum += generation
            avg = _sum/5
            median = (min(generations, key=lambda x: abs(x - avg)))
            index = generations.index(median) + 1
            print("median = %s, index: %s" % (median, index))
            print("---------------------------------------------")
            for i in range(1, 6):
                filename = "perm/len" + str(LEN) + "-p" + str(population) + "-m" + str(mutation) + "-e" + str(elitism) + "-" + str(i) + ".csv"
                if i != index and os.path.exists(filename):
                    os.remove(filename)  
                    
if __name__ == "__main__":
    GA = GeneticAlgorithm()
    pop = [10, 20, 40]
    mut = [0.04, 0.08, 0.16]
    elite = [1, 2, 4]
    
    attempt_count = 0
    
    for j in range(3):
        k = 0
        while k < 5:
            k += 1
            solution = GA.run(pop[j], mut[0], elite[0], k)
            attempt_count+=1
            if not solution:
                k -= 1
    for j in range(1, 3):
        k = 0
        while k < 5:
            k += 1
            solution = GA.run(pop[0], mut[j], elite[0], k)
            attempt_count+=1
            if not solution:
                k -= 1
    for j in range(1, 3):
        k = 0
        while k < 5:
            k += 1
            solution = GA.run(pop[0], mut[0], elite[j], k)
            attempt_count+=1
            if not solution:
                k -= 1
                
    print("TOTAL RUNS COMPLETED: " + str(attempt_count))