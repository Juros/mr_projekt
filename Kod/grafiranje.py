import os
import numpy as np
import matplotlib.pyplot as plt
import random

prefixed = [filename for filename in os.listdir(r"C:\Users\user\Desktop\Faks\MR\ProjektniZadatak\perm/") if filename.startswith("len")]

for file in prefixed:
    podaci=file.split("-")
    noQ=podaci[0]
    noP=podaci[1]
    mR=podaci[2]
    noE=podaci[3]
    n=podaci[4]
    n=n[:-4]
    n=int(n)
    
    xA,yA= np.loadtxt(r"C:\Users\user\Desktop\Faks\MR\ProjektniZadatak\perm/"+file, delimiter=',', unpack=True)
    
    plt.figure()
    plt.step(xA,yA, linestyle='--', color='r')
    plt.title('Mut= '+str(mR)+ ', Pop= ' +str(noP) + ', Elitni clanovi= '+str(noE))
    plt.xlabel('X=Broj generacija')
    plt.ylabel('Y=Vrijednost funkcije dobrote')
    plt.savefig(r'C:\Users\user\Desktop\Faks\MR\ProjektniZadatak\perm\grafovi/'+'_'+str(noQ)+'_'+str(noP)+'_'+str(noE)+'_'+str(mR)+'_per.png',bbox_inches='tight')
    plt.show()